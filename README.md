# BESSY/HZB bluesky shell scripts 

This repository contains collection shell scripts for various bluesky launcher tools and utilities.

**Achtung: the only command updated for the container is bluesky_start_root, all the others have not been changed and will not work.**

**/shell_scripts** repository expected to be cloned to the same path at the rest of the HZB bluesky components (normally `~/bluesky`).



## Internal structure and paths:

| path | comment |
|  :------ |:------ | 
| `~/bluesky/shell_scripts/bin` | User executable scripts. This path will added to $PATH variable |
| `~/bluesky/shell_scripts/etc` | Various configs and etc. At initial version here would be a template to be added to .bashrc |
| `~/bluesky/shell_scripts/tools` | Additional tools aimed for mainainance of the bluesky installation itself which are not not needed to run by a user |


## Installation

* clone this repository to ~/bluesky/shell_scripts path:

```shell
cd ~/bluesky
git clone https://gitlab.helmholtz-berlin.de/bessyII/bluesky/shell_scripts
```

* add following lines in `.profile` or `.bashrc`:

```bash

if [ -f ~/bluesky/shell_scripts/etc/profile ]; then
    . ~/bluesky/shell_scripts/etc/profile
fi
```

Use `.profile` if you want to have bluesky commands and paths added to interative shells only (suggested option) or `.bashrc` if you want it for all shell invocations.
